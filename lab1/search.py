# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util
import copy
from game import Directions

s = Directions.SOUTH
w = Directions.WEST
n = Directions.NORTH
e = Directions.EAST
p = Directions.STOP

class SearchNode:
    """
    This class represents a node in the graph which represents the search problem.
    The class is used as a basic wrapper for search methods - you may use it, however
    you can solve the assignment without it.

    REMINDER: You need to fill in the backtrack function in this class!
    """

    def __init__(self, position, parent=None, transition=None, cost=0, heuristic=0):
        """
        Basic constructor which copies the values. Remember, you can access all the 
        values of a python object simply by referencing them - there is no need for 
        a getter method. 
        """
        self.position = position
        self.parent = parent
        self.cost = cost
        self.heuristic = heuristic
        self.transition = transition

    def isRootNode(self):
        """
        Check if the node has a parent.
        returns True in case it does, False otherwise
        """
        return self.parent == None 

    def unpack(self):
        """
        Return all relevant values for the current node.
        Returns position, parent node, cost, heuristic value
        """
        return self.position, self.parent, self.cost, self.heuristic


    def backtrack(self):
        """
        Reconstruct a path to the initial state from the current node.
        Bear in mind that usually you will reconstruct the path from the 
        final node to the initial.
        """
        moves = []
        # make a deep copy to stop any referencing isues.
        node = copy.deepcopy(self)

        if node.isRootNode(): 
            # The initial state is the final state
            return moves        


        "**YOUR CODE HERE**"
        while (node.parent is not None):
            moves.insert(0, node.transition)
            node = node.parent
        return moves

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())
    """

    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem):
    return treeSearch(problem, dfs)

def breadthFirstSearch(problem):
    import searchAgents

    #hack hack hack
    switch = isinstance(problem, searchAgents.CornersProblem)
    if switch:
        return cornersSearch(problem)
    return treeSearch(problem, bfs)

def uniformCostSearch(problem):
    return treeSearch(problem, ucs)

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.~
    """
    return 0

def calculatePriority(problem, node, heuristic):
    node.heuristic = heuristic(node.position, problem)
    return node.cost + node.heuristic

def aStarSearch(problem, heuristic=nullHeuristic):
    initNode = SearchNode(problem.getStartState())
    open = util.PriorityQueue()
    open.push(initNode, calculatePriority(problem, initNode, heuristic))
    closed = []

    while (open.isEmpty() is False):
        n = open.pop()
        if problem.isGoalState(n.position):
            return n.backtrack()
        closed.append(n)

        for succ in problem.getSuccessors(n.position):
            succNode = SearchNode(succ[0], n, succ[1], succ[2] + n.cost)

            tempset1 = [x for x in closed if x.position == succNode.position]
            tempset2 = [trip[2] for trip in open.heap if trip[2].position == succNode.position]
            union = list(set().union(tempset1, tempset2))

            flag = True
            for candidate in union:
                if candidate.position == succNode.position:
                    if candidate.cost < succNode.cost:
                        flag = False
                        break
                    else:
                        for ind in range(len(closed)):
                            if closed[ind].position == succNode.position:
                                closed.pop(ind)
                                break
                        
                        for ind in range(len(open.heap)):
                            if open.heap[ind][2].position == succNode.position:
                                open.heap.pop(ind)
                                break

            if flag:
                pass
            else:
                continue

            if (succNode.position not in closed):
                closed.append(succNode)
                open.push(succNode, calculatePriority(problem, succNode, heuristic))


def treeSearch(problem, problemtype):
    initNode = SearchNode(problem.getStartState())
    open = None
    if problemtype == dfs:
        open = util.Stack()
    elif problemtype == bfs:
        open = util.Queue()
    else:
        open = util.PriorityQueue()
    open.push(initNode) if problemtype != ucs else open.push(initNode, initNode.cost)
    visited = set()

    while (open.isEmpty() is not True):
        n = open.pop()
        
        if n.position in visited:
            continue
        visited.add(n.position)

        if problem.isGoalState(n.position):
            return n.backtrack()
        
        for succ in problem.getSuccessors(n.position):
            succNode = SearchNode(succ[0], n, succ[1], succ[2] + n.cost)
            open.push(succNode) if problemtype != ucs else open.push(succNode, succNode.cost)

def cornersSearch(problem):
    initNode = SearchNode(problem.getStartState())
    initNode.position = ((initNode.position), 0)
    open = util.Queue()
    open.push(initNode)
    visited = set()

    while open.isEmpty is False:
        n = open.pop()
        sum = n.position[1]
        if n.position[1] in visited:
            continue
        visited.add(n.position[0])

        if problem.isGoalState(n.position):
            return n.backtrack() #todo dovrsiti
        
        for succ in problem.getSuccessors(n.position[0]):
            succNode = SearchNode(succ[0], n, succ[1], succ[2] + n.cost)
            open.push(succNode)


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
